package com.example.smv.findme.ui;

import com.example.smv.findme.R;
import com.example.smv.findme.model.NearByApiResponse;
import com.example.smv.findme.myapp.MyApplication;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import android.Manifest.permission;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback  {

    private GoogleMap mMap;
    private String type;
    private static int PROXIMITY_RADIUS = 300;
    FusedLocationProviderClient providerClient;
    private static final LatLngBounds BOUNDS_INDIA = new LatLngBounds(new LatLng(23.63936, 68.14712), new LatLng(28.20453, 97.34466));
    private double curentLat, curentLng;
    private ProgressDialog progressBar;


    @Override
    public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        return super.onCreateView( parent, name, context, attrs );

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_maps );


        providerClient = LocationServices.getFusedLocationProviderClient( this );
        myLastKnownLocatio();
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById( R.id.map );
        mapFragment.getMapAsync( this );




        progressBar = new ProgressDialog( MapsActivity.this );
        progressBar.setCancelable( true );
        progressBar.setMessage( "Please Wait..." );
        progressBar.setProgressStyle( ProgressDialog.STYLE_SPINNER );
        progressBar.setProgress( 0 );
        progressBar.setMax( 1000 );
        progressBar.show();


        Boolean yes = CheckGooglePlayServices();

        Log.i( "ORbjzkbkjvbsdvbsdd", "1st" );

        Log.i( "2121", String.valueOf( yes ) );





        type = getIntent().getStringExtra( "type" );

        Log.i( "2121", String.valueOf( type ) );


    }


    private boolean CheckGooglePlayServices() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable( this );
        if (result != ConnectionResult.SUCCESS) {
            if (googleAPI.isUserResolvableError( result )) {
                googleAPI.getErrorDialog( this, result,
                        0 ).show();
            }
            return false;
        }
        return true;
    }


    private void myLastKnownLocatio() {

        if (ActivityCompat.checkSelfPermission( this, permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission( this, permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        providerClient.getLastLocation()
                .addOnSuccessListener( MapsActivity.this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {


                        if (location != null) {
                            Log.i( "ORbjzkbkjvbsdvbsdd", "location" );
                            curentLat = location.getLatitude();
                            curentLng = location.getLongitude();
                            Log.i( "1231", "====>latMapActivity     " + curentLat );
                            Log.i( "1231", "====>lngMapActivity     " + curentLng );
                            findPlaces( type );
                        }


                    }


                } );
    }


    private void findPlaces(final String type) {
        Log.i( "ORbjzkbkjvbsdvbsdd", "findPlce" );

        Call<NearByApiResponse> call = MyApplication.getApp().getApiService().getNearbyPlaces( type, curentLat + "," + curentLng, PROXIMITY_RADIUS );

        call.enqueue( new Callback<NearByApiResponse>() {
            @Override
            public void onResponse(Call<NearByApiResponse> call, Response<NearByApiResponse> response) {
                try {
                    if (response.body().getStatus().contains("ZERO_RESULTS")){
                        PROXIMITY_RADIUS += 500;
                        findPlaces(type);
                    }
                    else {
//                    googleMap.clear();
                    // This loop will go through all the results and add marker on each location.
                    for (int i = 0; i < response.body().getResults().size(); i++) {
                        Double lat = response.body().getResults().get(i).getGeometry().getLocation().getLat();
                        Double lng = response.body().getResults().get(i).getGeometry().getLocation().getLng();
                        String placeName = response.body().getResults().get(i).getName();
                        String vicinity = response.body().getResults().get(i).getVicinity();
                        String icon = response.body().getResults().get(i).getIcon();

                        Log.i("123", "====>lat" + lat);
                        Log.i("123", "====>lng" + lng);
                        Log.i("123", "====>placeName" + placeName);
                        Log.i("123", "====>vicinity" + vicinity);

                        MarkerOptions markerOptions = new MarkerOptions();
                        LatLng latLng = new LatLng(lat, lng);
                        // Location of Marker on Map
                        markerOptions.position(latLng);
                        // Title for Marker
                        markerOptions.title(placeName + " : " + vicinity);
                        // Color or drawable for marker
                        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
                        // add marker
                        Marker m = mMap.addMarker(markerOptions);
                        // move map camera

                        progressBar.dismiss();
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
//                        mMap.animateCamera( CameraUpdateFactory.zoomTo( 13 ) );
                    }
                    }
                } catch (Exception e) {
                    Log.d( "onResponse", "There is an error" );
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<NearByApiResponse> call, Throwable t) {
                Log.d( "onFailure", t.toString() );
                t.printStackTrace();
                PROXIMITY_RADIUS += 10000;
            }
        } );


    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng( -34, 151 );
        if (ActivityCompat.checkSelfPermission( this, permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission( this, permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        mMap.setMyLocationEnabled( true );
        mMap.moveCamera( CameraUpdateFactory.newLatLngZoom(BOUNDS_INDIA.getCenter() ,16 ) );



    }
}
